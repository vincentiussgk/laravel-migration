<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreateUsersTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profil', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama lengkap', 100);
            $table->string('email', 45)->unique();
            $table->string('foto', 45);
        });

        Schema::create('pertanyaan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul', 45);
            $table->string('isi', 255);
            $table->timestamp('tanggal_dibuat');
            $table->timestamp('tanggal_diperbaharui');
            $table->integer('jawaban_tepat_id');
            $table->foreign('profil_id')->references('id')->on('profil');
        });

        Schema::create('like_dislike_pertanyaan', function (Blueprint $table) {
            $table->foreign('profil_id')->references('id')->on('profil');
            $table->foreign('pertanyaan_id')->references('id')->on('pertanyaan');
            $table->integer('poin', 11);
        });

        Schema::create('komentar_pertanyaan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('isi', 255);
            $table->timestamp('tanggal_dibuat')->unique();
            $table->foreign('pertanyaan_id')->references('id')->on('pertanyaan');
            $table->foreign('profil_id')->references('id')->on('profil');
        });

        Schema::create('jawaban', function (Blueprint $table) {
            $table->increments('id');
            $table->string('isi', 255);
            $table->timestamp('tanggal_dibuat');
            $table->timestamp('tanggal_diperbaharui');
            $table->foreign('pertanyaan_id')->references('id')->on('pertanyaan');
            $table->foreign('profil_id')->references('id')->on('profil');
        });

        Schema::create('komentar_jawaban', function (Blueprint $table) {
            $table->increments('id');
            $table->string('isi', 255);
            $table->timestamp('tanggal_dibuat');
            $table->foreign('jawaban_id')->references('id')->on('jawaban');
            $table->foreign('profil_id')->references('id')->on('profil');
        });

        Schema::create('like_dislike_jawaban', function (Blueprint $table) {
            $table->integer('poin', 11);
            $table->foreign('jawaban_id')->references('id')->on('jawaban');
            $table->foreign('profil_id')->references('id')->on('profil');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
